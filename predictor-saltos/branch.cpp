#include "functions.h"
#include "functions.cpp"

int main(int argc, char** argv){
    string trace, result, bp_chosen, history, phistory, ghistory;
    int size,s,bp,gh,ph,o;
    long pc_now;
    ulong index, index_pht;
    char out, pred;  
    long cN = 0; //cantidad de predicciones correctas de T y N
    long iN = 0;  //cantidad de predicciones incorrectas de T y N
    long iT = 0;
    long cT = 0;

    //contadores de predicciones correctas e incorrectas para el metapredictor 
    long cNm = 0; 
    long cTm = 0; 
    long iTm = 0; 
    long iNm = 0; 
    //se obtienen parametros 
    s = stoi(argv[2]); 
    bp = stoi(argv[4]);
    ph = stoi(argv[8]);
    gh = stoi(argv[6]); 

    
    size = pow(2, s);  //size of array

    char counter_array[size], counter_array_g[size], counter_array_p[size], metapredictor[size];//se define array de 2bc

    vector<string>pht(size); //se define pht

    initial(size, counter_array, 'N'); //se inicializan contadores (BHT)

    if(bp == 1 || bp == 3){ //si se va a usar el PShare o de Torneo
        string historia = "";
        for (int i = 0; i < ph; i++){
            historia += '0'; //se guardan 0's suponiendo que la salida ha sido N por mucho tiempo
        }
        for(int i = 0; i < size; i++){
            pht[i] = historia; 
        }
    }
    if (bp == 2 || bp == 3){ //si se va a usar el GSHare o de Torneo
        history = "";
        for(int i = 0; i < gh; i++){
            history += '0'; //se define el estado incial de la historia global
            if (bp == 3){
                ghistory = history; 
            }
        }

    }

    if(bp == 3){
        initial(size, counter_array_g, 'N'); //bht GShare 
        initial(size, counter_array_p,'N');  //bht PShare
        initial(size,metapredictor,'P'); //Tabla con 2bc del metapredictor
    }

    while (getline(cin,trace)){
        int pos = trace.find(" "); //para encontrar la posicion del espacio
        out = trace[pos+1]; //output real 
        //cout << out << endl;
        pc_now = stol(trace.substr(0,pos)); // el PC es lo que esta antes del espacio 
        
        if(bp == 0){
            //Predictor bimodal 
            bp_chosen = "Bimodal";
            index = get_index(s,pc_now); //se obtiene indice para acceso a array de 2bc
            pred = predict(counter_array, index); //se obtiene prediccion
            // for (int i = 0; i < size; i++) {
            //     cout << counter_array[i] ; 
            // }
            update(counter_array,index,pred,out,&cT,&cN,&iT,&iN,&result); //se actualizan contadores

            // cout <<" | "<< out <<" | "<< pred <<" | "<< result << endl;
            // cout << cT<<endl;

        }

        else if(bp == 1){
            //PShare
            bp_chosen = "PShare";
            index_pht = get_index(s,pc_now); //se obtiene indice para la PHT
            
            history = pht[index_pht];
            pred = pshare(&history,counter_array,s,pc_now,&index); //se hace prediccion
            // for (int i = 0; i < size; i++) {
            //     cout << counter_array[i] ; 
            // }
            update(counter_array,index,pred,out,&cT,&cN,&iT,&iN,&result); //se actualizan contadores
            history = pht[index_pht]; //bits de historia que se deben actualizar
            shift_left(&history, &out, bp, ph);//shift
            pht[index_pht] = history; //se actualizan bits de historia de tabla PHT
            
            // cout <<" | "<< out <<" | "<< pred <<" | "<< result << endl;


           

        }

        else if(bp == 2){
            //GShare 
            bp_chosen = "GShare"; 
            pred = gshare(&history,counter_array,s,pc_now,&index,gh); //se hace prediccion
            // for (int i = 0; i < size; i++) {
            //     cout << counter_array[i] ; 
            // }
            update(counter_array,index,pred,out,&cT,&cN,&iT,&iN,&result); //se actualizan contadores 
            shift_left(&history, &out, bp, gh );//shift

            // cout <<" | "<< out <<" | "<< pred <<" | "<< result << endl;

        }

        else if (bp == 3){
            //Predictor con Torneo
            bp_chosen = "Tournament"; 
            //GShare
            ulong indexg;
            char pred_g = gshare(&ghistory,counter_array_g,s,pc_now,&indexg,gh); //prediccion 
            update(counter_array_g,indexg,pred_g,out,&cT,&cN,&iT,&iN,&result); //se actualizan contadores GShare
            shift_left(&ghistory, &out, bp, gh );//shift

            //PShare
            ulong indexp;
            index_pht = get_index(s,pc_now); //se obtiene indice para la PHT
            phistory = pht[index_pht];
            char pred_p = pshare(&phistory,counter_array_p,s,pc_now,&indexp); //se hace prediccion
            
            phistory = pht[index_pht]; //bits de historia que se deben actualizar
            update(counter_array_p,indexp,pred_p,out,&cT,&cN,&iT,&iN,&result); //se actualizan contadores PShare
            shift_left(&phistory, &out, bp, ph);//shift
            pht[index_pht] = phistory; //se actualizan bits de historia de tabla PHT

            //Metapredictor
            index = get_index(s,pc_now);
           char pred_m = meta(metapredictor,index);//se obtiene prediccion
            if(pred_m == 'P'){
                pred = pred_p;
            }
            else{
                pred = pred_g;
            }
            update_meta(metapredictor,index,pred,pred_g,pred_p,out,&cTm,&cNm,&iTm,&iNm,&result,pred_m);
          


            // cout <<pc_now;
            // cout <<" | "<< pred_m <<" | "<< out <<" | "<< pred <<" "<< result <<endl;
        }  
    }
    if (bp == 3){
      print_results(cTm, cNm, iTm, iNm, bp,size,gh,ph,bp_chosen); //se imprimen resultados para metapredictor
    }
    if (bp != 3){    
        print_results(cT, cN, iT, iN, bp,size,gh,ph,bp_chosen); //se imprimen resultados para cualquier otro predictor
    
    }

    
    return 0; 
}