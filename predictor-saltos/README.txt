Tarea Predictores de saltos Estructuras de Compus 2

En el programa se podrán observar los resultados obtenidos con distintos predictores de saltos para un trace
con 16 millones de saltos condicionales. 

Carolina Paniagua Peñaranda - B65258

Para visualizar los resultados del predictor Bimodal, escribir en la línea de comandos: 
    make bimodal

Para visualizar los resultados del predictor con historia global, escribir en la línea de comandos: 
    make gshare

Para visualizar los resultados del predictor con historia privada, escribir en la línea de comandos: 
    make pshare

Para visualizar los resultados del predictor con torneo escribir en la línea de comandos: 
    make tournament

Para eliminar los archivos generados, escribir en la línea de comandos: 
    make clean