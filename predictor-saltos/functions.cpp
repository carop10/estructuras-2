
// Funcion para inicializar los 2bc 
//Parametros: 
// puntero a direccion incial de bht 
// tamaño de tabla (array)
// estado inicial de contadores
void initial(int size, char* array, char state){
    for(int i = 0; i < size; i++){
        *array = state;
        array++;
    }
}




//Funcion que retorna los ultimos n bits del pc en decimal para hacer un acceso a una tabla 
//Parametros: 
//n: cantidad de bits que se toman en cuenta para el indice
//pc_now: Direccion de PC obtenida del trace
//Retorno: indice para arrays
ulong get_index(int n, long pc_now){
    string pc_bin = bitset<32>(pc_now).to_string(); //PC en binario (32 bits)
    //cout << "Pc en binario:" << pc_bin << endl;
    string index = pc_bin.substr(sizeof(pc_bin)-n); //el indice corresponde a los ultimos n bits del PC
    // cout << "Index:" << bitset<32>(index).to_ulong()<< endl;
    return (bitset<32>(index).to_ulong()); //se retorna index en decimal como un ulong
}


//Funcion que retorna prediccion para un indice dado
//Parametros: 
//array donde se encuentran los 2bc 
//indice para array
//Retorno: prediccion hecha
char predict(char* array, ulong index){
    array += index; //para apuntar al elemento del array que nos interesa
    if (*array == 'T' || *array == 't'){
        //se predice un T si se esta en el estado strong o weakly taken
        return 'T'; 
    } 
    else if (*array == 'N' || *array == 'n'){
        //se predice un N si se esta en el estado strong o weakly not taken
        return 'N'; 
    }
}
//Funcion que revisa si la prediccion fue correcta, hace el update de los contadores y agrega 1 
//a la cantidad de predicciones correctas o incorrectas
//Parametros: 
//arreglo de caracteres al que se le hara un update
//indice del elemento del array al que se le hara un update
//prediccion realizada
//salida del programa 
// contadores para predicciones de saltos tomados y no tomados correctos e incorrectos
// un string para indicar si la prediccion fue correcta o incorrecta a la hora de realizar pruebas
void update(char* array, ulong index, char pred, char out,long* cT,long* cN,long* iT,long* iN, string* result){
    array += (int) index;
    
    if (pred == out){ //prediccion correcta
        *result = "correct";
        if(*array == 't'|| *array == 'T'){ //si el estado del contador era t o T
            *cT += 1; //se suma 1 a pred de saltos tomados correctos
            *array = 'T'; //se actualiza a strong taken
        }
        else if(*array == 'n'|| *array == 'N'){ //si el estado del contador era n o N
            *cN +=1; //se suma 1 a pred correctas de saltos no tomados 
            *array = 'N'; //se actualiza a strong not taken
        }
    }
    else if (pred != out){ //prediccion incorrecta
        
        *result = "incorrect";
        if(*array == 't'){ //estado weakly taken
            *iN +=1; //se suma 1 a predicciones incorrectas de saltos no tomados
            *array = 'n';  //se actualiza estado a weakly not taken
        }
        else if(*array == 'n'){ //estado weakly not taken
            *iT +=1; //se suma 1 a predicciones incorrectas de saltos tomados 
            *array = 't'; //se actualiza estado a weakly taken
        }
        else if(*array == 'T'){ //estado strong taken
            *iN+=1; //se suma 1 a predicciones incorrectas de saltos no tomados
            *array = 't'; //se actualiza a estado weakly taken
        }
        else if (*array == 'N'){ //estado strong not taken
            *iT+=1; //se suma 1 a predicciones incorrectas de saltos tomados
            *array = 'n'; //se actualiza a estado weakly not taken
        }
    }
    
}
//Funcion que calcula el indice de la bht para el GShare y realiza la prediccion
//Parametros: 
//Registro con bits de historia global 
//bht
//s
//gh
//PC actual 
//variable en donde se almacenara el valor del indice para la tabla bht
//Retorno: prediccion (T o N)
char gshare(string* ghistory, char* counter_array, int s, long pc_now, ulong* index, int gh){
    ulong ghistory_dec; 
    string xor_res; 
    ghistory_dec = bitset<32>(*ghistory).to_ulong(); //bits de historia en decimal
    xor_res = to_string(pc_now^ghistory_dec); //resultado de la XOR como string
    *index = get_index(s, stol(xor_res)); //se obtiene indices para 2bc
    return predict(counter_array,*index); //se obtiene prediccion
}

//Funcion que hace un shift hacia la izquierda de los bits de historia para guardar el ultimo salto
//Parametros: 
//bits de historia
//salida del programa 
//bp
//gh o ph (Sirve para hacer el shift en los bits de historia global o privada)
void shift_left(string* history, char* out, int bp, int gh){
    string shifted = *history;
        //se recorren los bits de historia y se hace un shift hacia la izquiera
        for(int i = 0; i < gh; i++){
            if (i != gh-1){ 
                shifted[i] = shifted[i+1];
            }
            else{ //si es el bit menos significativo, se guarda el salto actual
                if (*out == 'T'){
                    shifted[i] = '1';
                }
                else{
                    shifted[i] = '0';
                }
            }
        }
    
    *history = shifted;

}

//Funcion que calcula el indice de la bht para el PShare y realiza la prediccion
//Parametros: 
//elemento del array de pht que contiene bits de historia privada
//bht
//s
//PC actual 
//variable en donde se almacenara el valor del indice para la tabla bht
//Retorno: prediccion (T o N)
char pshare(string* p_history, char* counter_array, int s, long pc_now, ulong* index){
    ulong phistory_dec; 
    string xor_res; 
    phistory_dec= bitset<32>(*p_history).to_ulong();
    xor_res = to_string(pc_now^phistory_dec); //resultado de la XOR como string
    *index = get_index(s, stol(xor_res)); //se obtiene indices para 2bc
    return predict(counter_array,*index); //se obtiene prediccion
}


//Funcion que retorna prediccion que realiza el metapredictor
//Parametros: 
//array donde se encuentran los 2bc 
//indice para array
//Valor de retorno: Predictor escogido
char meta(char* array, ulong index){
    array += index;
    if (*array == 'P' || *array == 'p'){ //Estado del contador weakly o strong prefered PShare
        return 'P';
    } 
    else{
        return 'G'; //Estado del contador weakly o strong prefered GSHare
    }

}

//Funcion que hace un update de los contadores del metapredictor y cuenta la cantidad de predicciones correctas e incorrectas
//Parametros: 
//metapredictor 
//indice para accesar al elemento del metapredictor que nos interesa
//prediccion metapredictor
//prediccion GShare
//prediccion PShare
//cantidad de predicciones correctas e incorrectas
//solo se actualizan los contadores del metapredictor si la prediccion de uno de los predictores fue correcta y la del otro fue incorrecta
void update_meta(char* metapredictor,ulong index,char pred,char pred_g,char pred_p,char out,long* cTm,long* cNm,long* iTm,long* iNm, string* result, char pred_m){
    metapredictor += index;
    if(pred == out){ 
        *result = "correct";
        if (out == 'T'){
            *cTm += 1; 
        }
        else if (out == 'N'){
            *cNm += 1; 
        }
        if(pred == pred_p & pred!= pred_g){
            if(*metapredictor == 'p'){
                *metapredictor = 'P'; 
            }
        }
        else if(pred_m == pred_g & pred != pred_p){
            if(*metapredictor == 'g'){
                *metapredictor = 'G'; 
            }
        }
    }

    else if(pred != out){
        *result = "incorrect";
        if (out == 'T'){
            *iTm += 1; 
        }
        else if (out == 'N'){
            *iNm += 1; 
        }
        if(pred == pred_p & pred != pred_g){
            if(*metapredictor == 'P'){
                *metapredictor = 'p'; 
            }
            else if(*metapredictor == 'p'){
                *metapredictor = 'g'; 
            }
        }
        else if(pred == pred_g & pred != pred_p){
            if(*metapredictor == 'g'){
                *metapredictor = 'p'; 
            }
            else if(*metapredictor == 'G'){
                *metapredictor = 'g'; 
            }

        }
    }
}

// Funcion que imprime los resultados del predictor de saltos seleccionado en terminal
// Parametros: 
// contadores de predicciones de saltos tomados y no tomados correctas e incorrectas
// bp,gh,ph
//predictor escogido

void print_results(long cT, long cN, long iT,long iN,int bp,int size,int gh,int ph, string bp_chosen){
    double percentage = (double(cT+cN)/double(cN+cT+iT+iN))*100; //se calcula el procentaje
    long total = cN+cT+iT+iN; //se calcula el total de saltos
    cout <<"-----------------------------------------------------------------------------------------------"<<endl;
    cout <<"Prediction parameters:"<<endl;
    cout <<"-----------------------------------------------------------------------------------------------"<<endl;
    cout <<"Branch prediction type:                                                "<<bp_chosen<<endl;
    cout <<"BHT size (entries):                                                    "<<size<<endl;
    cout <<"Global history register size:                                          "<<gh<<endl;
    cout <<"Private history register size:                                         "<<ph<<endl;
    cout <<"-----------------------------------------------------------------------------------------------"<<endl;
    cout <<"Simulation results:"<<endl;
    cout <<"-----------------------------------------------------------------------------------------------"<<endl;
    cout <<"Number of branch:                                                      "<<total<<endl;
    cout <<"Number of correct prediction of taken branches:                        "<<cT<<endl;
    cout <<"Number of incorrect prediction of taken branches:                      "<<iT<<endl;
    cout <<"Correct prediction of not taken branches:                              "<<cN<<endl;
    cout <<"Incorrect prediction of not taken branches:                            "<<iN<<endl;
    cout <<"Percentage of correct predictions                                      "<<percentage<<endl;
    cout <<"-----------------------------------------------------------------------------------------------"<<endl;
}
