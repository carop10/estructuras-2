#include <iostream>
#include <cstdlib>
#include <string>
#include <cmath>
#include <vector>
#include <bitset>
#include <iterator>
#include <algorithm> 
#include <bits/stdc++.h> 

using namespace std;

void initial(int size, char* array, char state);
ulong get_index(int n, long pc_now);
char predict(char* array, ulong index);
void update(char* array, ulong index, char pred, char out,long* cT,long* cN,long* iT,long* iN, string* result);
char gshare(string* ghistory, char* counter_array, int s, long pc_now, ulong* index, int gh);
void shift_left(string* history, char* out, int bp, int gh);
char pshare(string* p_history, char* counter_array, int s, long pc_now, ulong* index);
char meta(char* array, ulong index);
void update_meta(char* metapredictor,ulong index,char pred,char pred_g,char pred_p,char out,long* cTm,long* cNm,long* iTm,long* iNm, string* result, char pred_m);
void print_results(long cT, long cN, long iT,long iN,int bp,int size,int gh,int ph, string bp_chosen);
